<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Op_propaccategories extends Module
{

    protected $images_dir;

    public function __construct()
    {
        $this->name = 'op_propaccategories';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'OrangePix';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->images_dir = $this->local_path.'views/img/banner/';
        $this->displayName = $this->l('OrangePix Custom Categories List');
        $this->description = $this->l('Custom Categories List for FrontEnd');

        $this->confirmUninstall = $this->l('Are you sure uninstall this module');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');

        // Create image path if not exist
        if (!file_exists($this->images_dir)){
            mkdir($this->images_dir,0777,true);
        }

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader');

    }

    public function uninstall()
    {
        //remove images folder
        if (file_exists($this->images_dir)){
            if (PHP_OS === 'Windows')
            {
                exec("rd /s /q {$this->images_dir}");
            }
            else
            {
                exec("rm -rf {$this->images_dir}");
            }
        }

        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitConfiguration')) == true) {
            $this->postProcess();
        }
        if (((bool)Tools::isSubmit('submitCategoryTree')) == true) {
            $this->postProcessCategories();
        }

        $token = Tools::getAdminTokenLite('AdminModules');
        $action_form = $this->context->link->getAdminLink('AdminModules', false)
          .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token='.$token;
        $this->context->smarty->assign('action_form',$action_form);
        $this->context->smarty->assign('banner',Configuration::get('PROPAC_CATEGORIES_BANNER'));

        $link = $this->context->link->getModuleLink( $this->name, 'Categories');
        $this->context->smarty->assign('categories_link',$link);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');
        $output .= $this->renderForm();
        $output .= $this->renderFormCategories();

        return $output;
    }

    public function renderForm()
    {
        $radio = (Tools::version_compare(_PS_VERSION_, '1.6', '>=')) ? 'switch' : 'radio';
        $multilingual = (bool)$this->getConfigurationValues()['multilingual'];
        $fields_form = array(
          'form' => array(
            'legend' => array(
              'title' => $this->l('Configuration'),
              'icon' => 'icon-cogs',
            ),
            'input' => array(
              array(
                'type' => $radio,
                'lang' => false,
                'label' => $this->l('Multilingual Form'),
                'is_bool' => true,
                'col' => 6,
                'name' => 'multilingual',
                'values' => array(
                  array(
                    'id' => 'active_on',
                    'value' => 1,
                    'label' => $this->l('Yes')
                  ),array(
                    'id' => 'active_off',
                    'value' => 0,
                    'label' => $this->l('No')
                  ),

                ),

              ),
              array(
                'type' => $radio,
                'lang' => false,
                'label' => $this->l('Enable Banner on FrontOffice'),
                'is_bool' => true,
                'col' => 6,
                'name' => 'enable-banner',
                'values' => array(
                  array(
                    'id' => 'active_on',
                    'value' => 1,
                    'label' => $this->l('Yes')
                  ),array(
                    'id' => 'active_off',
                    'value' => 0,
                    'label' => $this->l('No')
                  ),

                ),

              ),
              array(
                'type' => 'file',
                'lang' => false,
                'label' => $this->l('Upload Image Banner'),
                'name' => 'banner',
                'display_image' => true,
                'accept' => "image/*",
                'has_filter' => true,
                'col' => 6,
              ),
              array(
                'type' => 'text',
                'lang' => $multilingual,
                'label' => $this->l('URL Banner'),
                'name' => 'url-banner',
                'col' => 6,
              ),
              array(
                'type' => 'textarea',
                'lang' => $multilingual,
                'label' => $this->l('Introduction Text'),
                'name' => 'introduction-text',
                'col' => 6,
              ),

            ),
            'submit' => array(
              'title' => $this->l('Save'),
            ),
          ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitConfiguration';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
          'fields_value' => $this->getConfigurationValues(),
          'languages' => $this->context->controller->getLanguages(),
          'id_language' => $this->context->language->id,
        );
        return $helper->generateForm(array($fields_form));
    }

    public function renderFormCategories()
    {
        $root = Category::getRootCategory();
        $selected_categories = json_decode(Configuration::get('PROPAC_CATEGORIES_TREE'),true);
        $selected_categories = is_array($selected_categories) ? array_map('intval',$selected_categories) : [1];
        //Generating the tree
        $tree = new HelperTreeCategories('categories_1'); //The string in param is the ID used by the generated tree
        $tree->setUseCheckBox(true)
          ->setAttribute('is_category_filter', $root->id)
          ->setRootCategory($root->id)
          ->setSelectedCategories( $selected_categories)
          ->setInputName('categories-tree');
        $categoryTree = $tree->render();


        $fields_form = array(
          'form' => array(
            'legend' => array(
              'title' => $this->l('Configuration'),
              'icon' => 'icon-cogs',
            ),
            'input' => array(
              array(
                'type' => 'categories_select',
                'label' => $this->l('Sceglie le Categorie visibile nel FrontOffice'),
                'col' => 6,
                'name' => 'categories-tree',
                'category_tree'  => $categoryTree,
              ),

            ),
            'submit' => array(
              'title' => $this->l('Save'),
            ),
          ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitCategoryTree';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        return $helper->generateForm(array($fields_form));
    }

    public function postProcess(){
        Configuration::updateValue('PROPAC_MULTILINGUAL_FORM',Tools::getValue('multilingual'));
        Configuration::updateValue('PROPAC_ENABLE_BANNER',Tools::getValue('enable-banner'));
        if ((bool)Tools::getValue('multilingual') == true){
            $text_values = array();
            $banner_values = array();
            foreach (Language::getLanguages() as $lang) {
                $text_values[$lang['id_lang']] = Tools::getValue('introduction-text_'.$lang['id_lang']);
                $banner_values[$lang['id_lang']] = Tools::getValue('url-banner_'.$lang['id_lang']);
            }
            Configuration::updateValue('PROPAC_INTRODUCTION_TEXT_CATEGORIES',$text_values);
            Configuration::updateValue('PROPAC_URL_BANNER',$banner_values);
        }else{
            Configuration::updateValue('PROPAC_INTRODUCTION_TEXT_CATEGORIES',Tools::getValue('introduction-text'));
            Configuration::updateValue('PROPAC_URL_BANNER',Tools::getValue('url-banner'));
        }


        if ((bool)Tools::isSubmit('banner') == true){
            $image = Tools::fileAttachment('banner');
            if (!empty($image)){
                // Create image path if not exist
                if (!file_exists($this->images_dir)){
                    mkdir($this->images_dir,0777,true);
                }
                move_uploaded_file($image['tmp_name'],$this->images_dir.$image['rename']);
                Configuration::updateValue('PROPAC_CATEGORIES_BANNER',$this->_path.'views/img/banner/'.$image['rename']);
            }
        }

    }

    public function postProcessCategories(){
        Configuration::updateValue('PROPAC_CATEGORIES_TREE',json_encode(Tools::getValue('categories-tree')));
    }

    public function getConfigurationValues(){
        $values = array(
          'introduction-text' => Configuration::get('PROPAC_INTRODUCTION_TEXT_CATEGORIES'),
          'multilingual' => Configuration::get('PROPAC_MULTILINGUAL_FORM'),
          'url-banner' => Configuration::get('PROPAC_URL_BANNER'),
          'enable-banner' => Configuration::get('PROPAC_ENABLE_BANNER'),
        );

        if ((bool)Configuration::get('PROPAC_MULTILINGUAL_FORM') == true){
            foreach (Language::getLanguages() as $lang) {
                $values['introduction-text'][$lang['id_lang']] = Configuration::get('PROPAC_INTRODUCTION_TEXT_CATEGORIES',$lang['id_lang']);
                $values['url-banner'][$lang['id_lang']] = Configuration::get('PROPAC_URL_BANNER',$lang['id_lang']);
            }
        }
        return $values;
    }

    public function getIntroductionText($id_lang=null){
        return Configuration::get('PROPAC_INTRODUCTION_TEXT_CATEGORIES',$id_lang == null ? $this->context->language->id : $id_lang);
    }


    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('configure') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addJS($this->_path.'views/js/bs-custom-file-input.min.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
       /* $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');*/
    }


}
