<?php


if (!defined('_PS_VERSION_')) {
    exit;
}

class op_propaccategoriesCategoriesModuleFrontController extends ModuleFrontController
{
    private $_template;
    private $_categories_list;


    public function __construct()
    {
        parent::__construct();

        $this->_template = 'module:op_propaccategories/views/templates/front/categories.tpl';
        $this->_categories_list = json_decode(Configuration::get('PROPAC_CATEGORIES_TREE'),true);
        $this->_categories_list = is_array($this->_categories_list) ? array_map('intval',$this->_categories_list) : [1];

    }

    public function initContent()
    {

        parent::initContent();
        if (Tools::version_compare(_PS_VERSION_,'1.7.6','>='))
            $this->setMedia(false);
        else
            $this->setMedia();

        $this->getCategoriesList();

    }

    public function getCategoriesList()
    {
        $categories = array();
        foreach ($this->_categories_list as $cat){
            $category = new Category($cat);
            $categories[$cat]['url'] = $category->getLink();
            $categories[$cat]['name'] = $category->getName($this->context->language->id);
            $categories[$cat]['link_rewrite'] = $category->link_rewrite[$this->context->language->id];
            $categories[$cat]['id_image'] = $category->id_image;
            $categories[$cat]['subcategories'] = $category->getSubCategories($this->context->language->id);
        }
        $this->context->smarty->assign('categories',$categories);
        $this->context->smarty->assign('banner',Configuration::get('PROPAC_CATEGORIES_BANNER'));
        $this->context->smarty->assign('introduction_text',$this->module->getIntroductionText($this->context->language->id));
        $url_banner = $this->module->getConfigurationValues()['url-banner'];
        $this->context->smarty->assign('url_banner',is_array($url_banner) ? $url_banner[$this->context->language->id] : $url_banner);
        $this->context->smarty->assign('enable_banner',(bool)$this->module->getConfigurationValues()['enable-banner']);

        $this->setTemplate($this->_template);
    }

}