<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{op_propaccategories}prestashop>op_propaccategories_d2c86ef940863cf7251927c2401f9e8c'] = 'Elenco di Categorie per FrontOffice';
$_MODULE['<{op_propaccategories}prestashop>op_propaccategories_04911cf3ac27849e3ed6aefb2e962ad0'] = 'Sei sicuro di volere disinstallare questo modulo';
$_MODULE['<{op_propaccategories}prestashop>op_propaccategories_254f642527b45bc260048e30704edb39'] = 'Configura';
$_MODULE['<{op_propaccategories}prestashop>op_propaccategories_0344f31d26786a80d849a7183c48c000'] = 'Campi multilingual';
$_MODULE['<{op_propaccategories}prestashop>op_propaccategories_93cba07454f06a4a960172bbd6e2a435'] = 'Si';
$_MODULE['<{op_propaccategories}prestashop>op_propaccategories_bafd7322c6e97d25b6299b5d6fe8920b'] = 'No';
$_MODULE['<{op_propaccategories}prestashop>op_propaccategories_da4f870adbe65582ae93421a797b89c0'] = 'Attiva il Banner in FrontOffice';
$_MODULE['<{op_propaccategories}prestashop>op_propaccategories_1deec3cc2d9a2aedbe99ba775e56c107'] = 'Carica il Banner';
$_MODULE['<{op_propaccategories}prestashop>op_propaccategories_9f10e06418a4933d5c206f44c117c667'] = 'Link del Banner';
$_MODULE['<{op_propaccategories}prestashop>op_propaccategories_50183b572bd9d21cbc46aed6062f8962'] = 'Testo iniziale';
$_MODULE['<{op_propaccategories}prestashop>op_propaccategories_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
