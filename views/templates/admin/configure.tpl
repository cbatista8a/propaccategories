{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<span style="margin: 10px;">{l s='FrontOffice Link' mod='op_propaccategories'} : <a href="{$categories_link}" target="_blank">{$categories_link}</a></span>
<div class="panel">
	<h3><i class="icon icon-picture"></i> {l s='Banner' mod='op_propaccategories'}</h3>

	<div style="text-align: center;">
		<img src="{$banner}" alt="Banner" style="max-height: 400px;" onerror="this.src='https://via.placeholder.com/400x400.png?text=Category+Banner'">
	</div>
</div>