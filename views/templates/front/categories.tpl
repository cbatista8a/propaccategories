{extends file=$layout}

{block name='content'}

    <section id="main">

        {block name='page_header_container'}
            {block name='page_title'}
                <header class="page-header">
                    <h1 class="tt-innerpagetitle">{l s='TUTTE LE CATEGORIE' mod='op_propaccategories'}</h1>
                </header>
            {/block}
        {/block}
{*TODO styling*}
        {block name='page_content_container'}
            <section id="content" class="page-content card card-block">
                {block name='page_content_top'}{/block}
                {block name='page_content'}
                    <div id="categorieprodotti" class="container products">
                        {if isset($banner) && $enable_banner}
                            <div class="banner row" style="text-align: center;">
                                <a class="" href="{$url_banner}">
                                    <img src="{$banner}" alt="Banner" title="Banner" style="max-height: 500px;" onerror="this.src='https://via.placeholder.com/728x90.png?text=Category+Banner'">
                                </a>
                            </div>
                        {/if}
                        <div class="row" style="text-align: center;width: 100%;margin-bottom: 20px;">
                            <span>{$introduction_text}</span>
                        </div>
                        <div class="product_list row" style="width: 100%;text-align: center;">
                            {foreach from=$categories item=category}
                                <div class="categorylist category-min-height col-xs-12 col-sm-6 col-md-6  col-lg-4 col-xl-4">
                                    <div class="catpro ">
                                        <div class="" style="margin-bottom: 10px;text-align: left;">
                                            <a class="thumbnail thumbnail-link" href="{$category.url}" title="{$category.name|escape:'html':'UTF-8'}" alt="{$category.name}">
                                                <img src="{$link->getCatImageLink($category.link_rewrite, $category.id_image, 'category_default')|escape:'html':'UTF-8'}" alt="{$category.name}" title="{$category.name}" style="width=180px;height: auto;" onerror="this.src='https://via.placeholder.com/141x180.png?text={$category.name}'">
                                            </a>
                                        </div>
                                        {if isset($category.subcategories)}
                                            <div class="Subcategories list-group" style="text-align: left;">
                                                <ul class="items-list" style="margin-left: 10%;">
                                                    <h3 class="product-title">
                                                        <a href="{$category.url}" title="{$category.name|escape:'html':'UTF-8'}"><strong>{$category.name|escape:'html':'UTF-8'}</strong></a>
                                                    </h3>
                                                    {foreach from=$category.subcategories item=subcategory}
                                                        <li class="item" style="list-style: disc;"> <a href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}">{$subcategory.name|escape:'html':'UTF-8'}</a></li>
                                                    {/foreach}
                                                </ul>
                                            </div>
                                        {/if}
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                {/block}
            </section>
        {/block}

        {block name='page_footer_container'}
            <footer class="page-footer">
                {block name='page_footer'}
                    <!-- Footer content -->
                {/block}
            </footer>
        {/block}

    </section>

{literal}
    <style>
        .thumbnail-link{    height: 100%;
            display: block;
            background-color: #efefef;}
        @media all and (min-width:544px){
            .category-min-height{min-height: 420px;}
        }
    </style>
{/literal}
{/block}


